from tkinter import *

def popup():
   def maj():
      global tv
      tv.set(e2.get())
      fInfos.destroy()

   fInfos = Toplevel()
   fInfos.title('Infos')
   fInfos.geometry('300x150+300+500')
   e2 = Entry(fInfos)
   e2.pack(padx=10, pady=10)
   Button(fInfos, text='Envoyer', command=maj).pack(padx=10, pady=10)
   Button(fInfos, text='Quitter', command=fInfos.destroy).pack(padx=10, pady=10)
   fInfos.transient(jeu)
   fInfos.grab_set()
   jeu.wait_window(fInfos)

jeu = Tk()
jeu.title('Fenêtre 1')
jeu.geometry('300x100+100+100')
tv = StringVar()
tv.set('Nom : ')
Label(jeu, textvariable = tv).pack(padx=10, pady=10)
Button(jeu, text='Entrer nom', command=popup).pack(padx=10, pady=10)

jeu.mainloop()