from data import *
from tkinter import Button
import pickle, os

# fonction qui affiche les lettre de l'alphabet
class Function(Data) :

    def __init__(self) :   
        pass

    def get_word_mask(lettre_trouves, complet_word) :
        word_mask = ""
        for letter in complet_word :
            if letter in lettre_trouves :
                word_mask += letter
            else :
                word_mask += "*"
        return word_mask

        
    def save_scores(scores) :
        score_file = open(Data().nom_fichier, 'wb')
        mon_pickler = pickle.Pickler(score_file)
        mon_pickler.dump(scores)
        score_file.close()

    def get_scores() :
        if os.path.exists(Data().nom_fichier) :
            score_file = open(Data().nom_fichier, 'rb')
            mon_depickler = pickle.Unpickler(score_file)
            scores = mon_depickler.load()
            score_file.close()
        else :
            scores = {}
        return scores 
