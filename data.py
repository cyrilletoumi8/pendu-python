
from tkinter import StringVar

class Data :
    
    letter_click_by_user = ""
    icon_path = "assets/img/icon.png"

    def __init__(self) :
        # Liste des mots
        self.mots = [
            "EQUATEUR",
            "ERYTHREE",
            "ESPAGNE",
            "ESTONIE",
            "CHILI",
            "CHINE",
            "CHYPRE",
            "COLOMBIE",
            "COMORES",
            "CONGO",
            "COREE",
            "COSTA",
            "BOTSWANA",
            "BRESIL",
            "BRUNEI",
            "BULGARIE",
            "BURKINA",
            "BURUNDI",
            "CAMBODGE",
            "CAMEROUN",
            "CANADA",
            "ARMENIE",
            "AUTRICHE",
            "BAHAMAS",
            "BAHREIN",
            "BARBADE",
            "BELGIQUE",
            "BELIZE",
            "BENIN",
            "BHOUTAN",
            "BIRMANIE",
            "BOLIVIE",
            "ISLANDE",
            "ISRAEL",
            "ITALIE",
            "JAMAIQUE",
            "JAPON",
            "JORDANIE",
            "KENYA",
            "KIRIBATI",
            "KOSOVO",
            "KOWEIT",
            "LAOS",
            "LESOTHO",
            "LETTONIE",
            "LIBAN",
            "LIBERIA",
            "LIBYE",
            "LITUANIE",
            "MACAO",
            "MALAISIE",
            "SENEGAL",
            "SERBIE",
            "SLOVENIE",
            "SOMALIE",
            "SOUDAN",
            "SUEDE",
            "SUISSE",
            "SURINAME",
            "SYRIE",
            "TAIWAN",
            "TANZANIE",
            "TCHAD",
            "TOGO",
            "TONGA",
            "TUNISIE",
            "TURQUIE",
            "TUVALU",
            "UKRAINE",
            "URUGUAY",
            "VANUATU",
            "ZAIRE",
            "ZAMBIE",
            "ZANZIBAR",
            "ZIMBABWE"
        ]

        # Nombre d'essaie possible
        self.essai = 8

        # Nom du fichier qui stocke le score des jeueurs
        self.nom_fichier = "assets/scores/score"