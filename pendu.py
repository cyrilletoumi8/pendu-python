# -*-coding:<<Latin-1>> -*

from tkinter import *
from tkinter.font import families, Font
from functions import *
from data import *
from random import choice
from tkinter.messagebox import askyesno

class Pendu(Function, Data) : 


    def __init__(self) :

        self.numero_image = 1

        scores = Function.get_scores()
        self.username = str()

        self.racine = Tk()
        self.racine.geometry("1100x600")
        self.racine.resizable(False, False)
        self.racine.title("PENDU")
        self.racine.iconphoto(False, PhotoImage(file=Data.icon_path))

        self.word_to_find = choice(Data().mots)
        self.lettre_trouves = []
        self.coup_essai = Data().essai
        self.word_mask = Function.get_word_mask(self.lettre_trouves, self.word_to_find) 

        def cancer() :
            rep = askyesno("Quitter", "Voulez-vous vraiment quitter ?")
            if rep :
                self.racine.quit()
                    
        def princip() :
            fenPrincip = Toplevel()
            fenPrincip.resizable(False, False)
            fenPrincip.title("Principe")
            fenPrincip.geometry("500x400-300-200")
            fenPrincip.configure(bg="#33E2B8")
            fenPrincip.iconphoto(False, PhotoImage(file=Data.icon_path))

            # fenPrincip.transient(self.racine)
            # fenPrincip.grab_set()
            # self.racine.wait_window(fenPrincip)

            lbPrincipe ="PRINCIPE : \nUn pays est choisi aléatoirement et vous \n"+\
                        "essayez de deviner les lettres qui constituent\n"+\
                        "l'orthographe de ce pays, une fause lettre vous\n"+\
                        "fais un essai de moins, si vous trouvez le nom\n"+\
                        "de ce pays avant épuisement de vos essais, \n"+\
                        "le nombre  essai restant constitue est considéré \ncomme vos points !\n"+\
                        "\n\nBONNE CHANCE !!!"

            Label(fenPrincip, text=lbPrincipe, textvariable="Buil By Tc Saga", bg="#33E2B8", font=ftComic1).pack(padx=10, pady=10)
            

        def about() :
            fenAbout = Toplevel()
            fenAbout.resizable(False, False)
            fenAbout.title("A Propos")
            fenAbout.geometry("500x400-300-200")
            fenAbout.configure(bg="#33E2B8")
            fenAbout.iconphoto(False, PhotoImage(file=Data.icon_path))
            fenAbout.transient(self.racine)
            fenAbout.grab_set()

            Label(fenAbout, bg="#33E2B8", text="*****  PENDU 1.0  *****", font=ftComic).pack(padx=10, pady=10, fill=X, side=TOP)
            # Label(fenAbout, bg="#33E2B8", image=PhotoImage(file=Data.icon_path), font=ftComic).pack(padx=10, pady=10, fill=X, side=TOP)
            
            Label(fenAbout, fg="blue", bg="#33E2B8", text="\n# By Tc Saga #", font=ftComic1).pack(padx=10, pady=10, fill=X, side=TOP)
            Label(fenAbout, bg="#33E2B8", text="Tel : 6 98 84 10 87", font=ftComic1).pack(padx=10, pady=10, fill=X, side=TOP)
            Label(fenAbout, bg="#33E2B8", text="Email : cyrilletoumi8@gmail.com", font=ftComic1).pack(padx=10, pady=10, fill=X, side=TOP)
            Label(fenAbout, bg="#33E2B8", text="@2021-2021 ", font=ftComic1).pack(padx=10, pady=10, fill=X, side=TOP)
            

            self.racine.wait_window(fenAbout)

        def restart() :
            self.word_to_find = choice(Data().mots)
            self.lettre_trouves = []
            self.coup_essai = Data().essai
            self.word_mask = Function.get_word_mask(self.lettre_trouves, self.word_to_find) 
            self.lettreSaisir.set(self.word_mask)
            self.nb_essai.set("Essai : "+str(self.coup_essai))

        # Définition du Menu
        menubar = Menu(self.racine)

        menu1 = Menu(menubar, tearoff=0)
        menu1.add_command(label="Recommencé  (F5)", command=restart)
        menu1.add_command(label="Principe  (F11)", command=princip)
        menu1.add_separator()
        menu1.add_command(label="Quitter (Esc)", command=cancer)
        menubar.add_cascade(label=" Jeu ", menu=menu1)

        # menu2 = Menu(menubar, tearoff=0)
        # menu2.add_command(label="Couper", command=alert)
        # menu2.add_command(label="Copier", command=alert)
        # menu2.add_command(label="Coller", command=alert)
        # menubar.add_cascade(label="Editer", menu=menu2)

        menu3 = Menu(menubar, tearoff=0)
        menu3.add_command(label="A propos  (F12)", command=about)
        menubar.add_cascade(label="Aide", menu=menu3)

        self.racine.config(menu=menubar, bg="#EEEEEE")

        # Definition des cadres (Frame)
        image = Frame(self.racine, bg="#C5C5EE", height=400)
        image.pack(side=RIGHT, padx=5, pady=5, fill=Y)

        displayWord = Frame(self.racine, bg="#00C5FF", height=150)
        displayWord.pack(side=TOP, padx=5, pady=5, fill=X)

        # Déclaration des variables Label
        tv = StringVar()
        tv.set('Nom du joueur : ')

        self.lettreSaisir = StringVar()
        self.lettreSaisir.set(self.word_mask)

        self.nb_essai = StringVar()
        self.nb_essai.set("Essai : "+str(self.coup_essai))

        self.lbScore = StringVar()
        self.lbScore.set("Score total : ")
        
        # Panneau du clavier
        playing = Frame(self.racine, height=500, width=800, bg="#FFFFFF" )
        playing.pack(side=RIGHT, padx=5, pady=5, fill="both")

        ftComic = Font (family = "Comic Sans MS", size = 20, underline = False, weight = "bold")
        ftComic1 = Font (family = "Comic Sans MS", size = 15, underline = False, weight = "bold")
        ftComic2 = Font (family = "Comic Sans MS", size = 9, underline = False, weight = "bold")

        # Affichage du mot masqué
        word = Label(displayWord, bg="#00C5FF", fg="darkred", textvariable=self.lettreSaisir, font=ftComic )
        Label(displayWord, bg="#00C5FF", text="L'orthographe du Mot :", font=ftComic ).pack(side="left", padx=5, pady=5)
        word.pack(side="left")
        Label(displayWord, bg="#00C5FF", textvariable=self.nb_essai, font=ftComic1).pack(side=RIGHT, padx=5, pady=5)
        
        # L'image du pendu
        img = PhotoImage(file="assets/img/7.png")
        lbImg = Label(image, image=img).pack(side=BOTTOM)

        bestWinner = str()
        for cle, valeur in scores.items() :
            bestWinner += cle+" : "+str(valeur)+" points\n"
            
        Label(image, bg="#C5C5EE", textvariable=tv, font=ftComic1).pack(side=TOP, padx=10, pady=10)
        Label(image, bg="#C5C5EE", textvariable=self.lbScore, font=ftComic1).pack(side=TOP, padx=10)
        Label(image, bg="#C5C5EE", text="\nMeilleurs joueurs", font=ftComic1).pack(side=TOP, padx=10)
        Label(image, bg="#C5C5EE", text=bestWinner, font=ftComic1).pack(side=TOP, padx=10)
        
        # Récuperation du nom de l'utilisateur
        def maj() :
            self.username = e2.get().capitalize()
            if len(self.username) > 3 :
                tv.set('Nom du joueur : '+self.username)
                fInfos.destroy()
                if self.username not in scores.keys() :
                    scores[self.username] = 0
                    self.lbScore.set("Score total : "+str(scores[self.username]))
                else : 
                    self.lbScore.set("Score total : "+str(scores[self.username]))
            else :
                pass 

        fInfos = Toplevel()
        fInfos.resizable(False, False)
        fInfos.title("Nom du Joueur")
        fInfos.geometry("300x100-300-200")
        fInfos.iconphoto(False, PhotoImage(file=Data.icon_path))

        e2 = Entry(fInfos, width=150, justify='center', font=ftComic1)
        e2.pack(padx=10, pady=10)
        Button(fInfos, text="Envoyer", activebackground="darkblue", activeforeground="white", height=3, relief="flat", bg="blue", fg="white", command=maj).pack(padx=10, pady=10, side=BOTTOM, fill=X)
        fInfos.transient(self.racine)
        fInfos.grab_set()
        self.racine.wait_window(fInfos)
       
        Button(playing, activebackground="darkblue", bg="blue", font=ftComic2, command= lambda : getClickLetter("A"), activeforeground="white", fg="white",relief='flat', text="A", width=6, height=3).grid(row=1, column=1, padx=10, pady=10)
        Button(playing, activebackground="darkblue", bg="blue", font=ftComic2, command= lambda : getClickLetter("Z"), activeforeground="white", fg="white",relief='flat', text="Z", width=6, height=3).grid(row=1, column=2, padx=10, pady=10)
        Button(playing, activebackground="darkblue", bg="blue", font=ftComic2, command= lambda : getClickLetter("E"), activeforeground="white", fg="white",relief='flat', text="E", width=6, height=3).grid(row=1, column=3, padx=10, pady=10)
        Button(playing, activebackground="darkblue", bg="blue", font=ftComic2, command= lambda : getClickLetter("R"), activeforeground="white", fg="white",relief='flat', text="R", width=6, height=3).grid(row=1, column=4, padx=10, pady=10)
        Button(playing, activebackground="darkblue", bg="blue", font=ftComic2, command= lambda : getClickLetter("T"), activeforeground="white", fg="white",relief='flat', text="T", width=6, height=3).grid(row=1, column=5, padx=10, pady=10)
        Button(playing, activebackground="darkblue", bg="blue", font=ftComic2, command= lambda : getClickLetter("Y"), activeforeground="white", fg="white",relief='flat', text="Y", width=6, height=3).grid(row=1, column=6, padx=10, pady=10)
        Button(playing, activebackground="darkblue", bg="blue", font=ftComic2, command= lambda : getClickLetter("U"), activeforeground="white", fg="white",relief='flat', text="U", width=6, height=3).grid(row=1, column=7, padx=10, pady=10)
        Button(playing, activebackground="darkblue", bg="blue", font=ftComic2, command= lambda : getClickLetter("I"), activeforeground="white", fg="white",relief='flat', text="I", width=6, height=3).grid(row=1, column=8, padx=10, pady=10)
        Button(playing, activebackground="darkblue", bg="blue", font=ftComic2, command= lambda : getClickLetter("O"), activeforeground="white", fg="white",relief='flat', text="O", width=6, height=3).grid(row=1, column=9, padx=10, pady=10)
        Button(playing, activebackground="darkblue", bg="blue", font=ftComic2, command= lambda : getClickLetter("P"), activeforeground="white", fg="white",relief='flat', text="P", width=6, height=3).grid(row=1, column=10, padx=10, pady=10)
        Button(playing, activebackground="darkblue", bg="blue", font=ftComic2, command= lambda : getClickLetter("Q"), activeforeground="white", fg="white",relief='flat', text="Q", width=6, height=3).grid(row=2, column=1, padx=10, pady=10)
        Button(playing, activebackground="darkblue", bg="blue", font=ftComic2, command= lambda : getClickLetter("S"), activeforeground="white", fg="white",relief='flat', text="S", width=6, height=3).grid(row=2, column=2, padx=10, pady=10)
        Button(playing, activebackground="darkblue", bg="blue", font=ftComic2, command= lambda : getClickLetter("D"), activeforeground="white", fg="white",relief='flat', text="D", width=6, height=3).grid(row=2, column=3, padx=10, pady=10)
        Button(playing, activebackground="darkblue", bg="blue", font=ftComic2, command= lambda : getClickLetter("F"), activeforeground="white", fg="white",relief='flat', text="F", width=6, height=3).grid(row=2, column=4, padx=10, pady=10)
        Button(playing, activebackground="darkblue", bg="blue", font=ftComic2, command= lambda : getClickLetter("G"), activeforeground="white", fg="white",relief='flat', text="G", width=6, height=3).grid(row=2, column=5, padx=10, pady=10)
        Button(playing, activebackground="darkblue", bg="blue", font=ftComic2, command= lambda : getClickLetter("H"), activeforeground="white", fg="white",relief='flat', text="H", width=6, height=3).grid(row=2, column=6, padx=10, pady=10)
        Button(playing, activebackground="darkblue", bg="blue", font=ftComic2, command= lambda : getClickLetter("J"), activeforeground="white", fg="white",relief='flat', text="J", width=6, height=3).grid(row=2, column=7, padx=10, pady=10)
        Button(playing, activebackground="darkblue", bg="blue", font=ftComic2, command= lambda : getClickLetter("K"), activeforeground="white", fg="white",relief='flat', text="K", width=6, height=3).grid(row=2, column=8, padx=10, pady=10)
        Button(playing, activebackground="darkblue", bg="blue", font=ftComic2, command= lambda : getClickLetter("L"), activeforeground="white", fg="white",relief='flat', text="L", width=6, height=3).grid(row=2, column=9, padx=10, pady=10)
        Button(playing, activebackground="darkblue", bg="blue", font=ftComic2, command= lambda : getClickLetter("M"), activeforeground="white", fg="white",relief='flat', text="M", width=6, height=3).grid(row=2, column=10, padx=10, pady=10)
        Button(playing, activebackground="darkblue", bg="blue", font=ftComic2, command= lambda : getClickLetter("W"), activeforeground="white", fg="white",relief='flat', text="W", width=6, height=3).grid(row=3, column=3, padx=10, pady=10)
        Button(playing, activebackground="darkblue", bg="blue", font=ftComic2, command= lambda : getClickLetter("X"), activeforeground="white", fg="white",relief='flat', text="X", width=6, height=3).grid(row=3, column=4, padx=10, pady=10)
        Button(playing, activebackground="darkblue", bg="blue", font=ftComic2, command= lambda : getClickLetter("C"), activeforeground="white", fg="white",relief='flat', text="C", width=6, height=3).grid(row=3, column=5, padx=10, pady=10)
        Button(playing, activebackground="darkblue", bg="blue", font=ftComic2, command= lambda : getClickLetter("V"), activeforeground="white", fg="white",relief='flat', text="V", width=6, height=3).grid(row=3, column=6, padx=10, pady=10)
        Button(playing, activebackground="darkblue", bg="blue", font=ftComic2, command= lambda : getClickLetter("B"), activeforeground="white", fg="white",relief='flat', text="B", width=6, height=3).grid(row=3, column=7, padx=10, pady=10)
        Button(playing, activebackground="darkblue", bg="blue", font=ftComic2, command= lambda : getClickLetter("N"), activeforeground="white", fg="white",relief='flat', text="N", width=6, height=3).grid(row=3, column=8, padx=10, pady=10)

        
        def getClickLetter(letter=None) :

            if letter in self.word_to_find :
                self.lettre_trouves.append(letter)
            elif self.coup_essai > 0 :
                self.coup_essai -= 1

                self.nb_essai.set("Essai : "+str(self.coup_essai))
                
            else :
                pass
            
            if self.coup_essai == 0 :
                response = askyesno("Perdu", "Vous avez perdus. Recommencé une nouvelle partie ?")
                if response :
                    restart()

                
             
            self.word_mask = Function.get_word_mask(self.lettre_trouves, self.word_to_find)
            self.lettreSaisir.set(self.word_mask)

            if self.word_mask == self.word_to_find :
                scores[self.username] += self.coup_essai
                Function.save_scores(scores)
                self.lbScore.set("Score total : "+str(scores[self.username]))

                response = askyesno("Gagné", "Vous avez gagnés {} points. Recommencé une nouvelle partie ?".format(self.coup_essai))
                if response :
                    restart()
        
        def actualiser(event) :
            t=event.keysym
            if t == "F5" : restart()
            elif t == "F11" : princip()
            elif t == "F12" : about()
            elif t == "Escape" : cancer()
            elif t.upper() == "A" : getClickLetter("A")
            elif t.upper() == "B" : getClickLetter("B")
            elif t.upper() == "C" : getClickLetter("C")
            elif t.upper() == "D" : getClickLetter("D")
            elif t.upper() == "E" : getClickLetter("E")
            elif t.upper() == "F" : getClickLetter("F")
            elif t.upper() == "G" : getClickLetter("G")
            elif t.upper() == "H" : getClickLetter("H")
            elif t.upper() == "I" : getClickLetter("I")
            elif t.upper() == "J" : getClickLetter("J")
            elif t.upper() == "K" : getClickLetter("K")
            elif t.upper() == "L" : getClickLetter("L")
            elif t.upper() == "M" : getClickLetter("M")
            elif t.upper() == "N" : getClickLetter("N")
            elif t.upper() == "O" : getClickLetter("O")
            elif t.upper() == "P" : getClickLetter("P")
            elif t.upper() == "Q" : getClickLetter("Q")
            elif t.upper() == "R" : getClickLetter("R")
            elif t.upper() == "S" : getClickLetter("S")
            elif t.upper() == "T" : getClickLetter("T")
            elif t.upper() == "U" : getClickLetter("U")
            elif t.upper() == "V" : getClickLetter("V")
            elif t.upper() == "W" : getClickLetter("W")
            elif t.upper() == "X" : getClickLetter("X")
            elif t.upper() == "Y" : getClickLetter("Y")
            elif t.upper() == "Z" : getClickLetter("Z")
            else : pass
        
        self.racine.bind("<Key>", actualiser)

        self.racine.mainloop()
        


# Programme principal
Pendu()